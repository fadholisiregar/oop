<?php 

require('Animal.php');
require('Frog.php');
require('Ape.php');

$sheep = new Animal("shaun");

echo $sheep->name . '<br>';; // "shaun"
echo $sheep->legs . '<br>';; // 2
echo $sheep->cold_blooded . '<br>';; // false
echo '<br>';

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
$sungokong = new Ape("kera sakti");
echo $sungokong->name . '<br>'; // "shaun"
echo $sungokong->legs . '<br>'; // 2
echo $sungokong->cold_blooded . '<br>';
$sungokong->yell(); // "Auooo"

echo '<br>';
echo '<br>';

$kodok = new Frog("buduk");
echo $kodok->name . '<br>'; // "shaun"
echo $kodok->legs . '<br>'; // 2
echo $kodok->cold_blooded . '<br>';
$kodok->jump(); // "hop hop"

?>