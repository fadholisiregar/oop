<?php 

// class Animal yang memiliki contructor name, default property legs = 2 dan cold_blooded = false
class Animal {
    public $name;
    public $legs = 2;
    public $cold_blooded = "false";
    public function __construct($name)
    {
        $this->name = $name;
    }
}

?>